package Vehiculo;
import java.util.LinkedList;

public class Autobus extends Vehiculo{
	private int cantPasajeros = 0;
	private LinkedList<Persona> pasajeros = new LinkedList<>();
	private static int aux;
	
	public Autobus(double km, Persona chofer) {
		super(km, chofer);
	}
	
	public void subirPasajero(Persona nuevoPasajero){
		if(cantPasajeros<30) {
			if(aux != 0) {	
			pasajeros.add(aux, nuevoPasajero);
			cantPasajeros++;
			}
			else {
				pasajeros.add(nuevoPasajero);
				cantPasajeros++;
			}
		}
		else {
			System.out.println("El autobus esta lleno.");
		}
	}
	
	public void bajarPasajero(int numPasajero){
		this.pasajeros.remove(numPasajero);
		cantPasajeros--;
		this.aux=numPasajero;
	}
	 
	public void cambiarChofer(Persona chofer) {
		
		if(cantPasajeros == 0) {
			this.asignarChofer(chofer);
			System.out.println("Se cambio el chofer.");
		}
		else {
			System.out.println("Hay Pasajeros.");
			System.out.println("No se puede cambiar el chofer.");
		}
		
		
	}

	@Override
	public String toString() {
		return "Autobus [Pasajero =" + pasajeros + "]";
	}


}
