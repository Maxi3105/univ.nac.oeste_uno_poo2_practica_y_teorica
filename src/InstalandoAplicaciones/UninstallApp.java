package InstalandoAplicaciones;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class UninstallApp {
	private  int cantidadDeAplicacionesInstaladas;
	private double cantidadDeMemoriaNecesaria;
	public ArrayList <Double> mbDeAplicacion;
	private int i;
	private int j;
	
	
	
public UninstallApp(String direccion) throws NumberFormatException, IOException, RestriccionVioladaException {
		super();
		this.mbDeAplicacion =new ArrayList<Double>();
		leerArchivo(direccion);
		int minimo=cantidadMinimaDeAppABorrar();
		System.out.println(minimo);
		crearArchivosSalida(minimo);
		
	}



/*
 * Metodo para la lectura del archivo almacenamiento.in*/	
	



public  void leerArchivo(String direccion) throws NumberFormatException, IOException, RestriccionVioladaException {
		
	File f = new File(direccion);
	
	Scanner entrada = new Scanner(f);
	    
 	    
 	    
 	    //leemos la primer entrada del archivo que corresponde a la cantidad de aplicaciones instaladas
 	   this.cantidadDeAplicacionesInstaladas= entrada.nextInt();
 	  
	    //leemos la segunda entrada entrada del archivo que corresponde a la cantidad de Memoria que necesitamos.
 	   this.cantidadDeMemoriaNecesaria= entrada.nextDouble();
 	  
 	  //creamos un array para ingresar los datos de el espacio que ocupan en memoria las aplicaciones instaladas
 	    
 	 
 	    
 	  //cargamos el array  
 	    
		while(entrada.hasNextDouble()) {
			if(entrada.nextDouble()<0){
				throw new RestriccionVioladaException("No se permiten apps con Mb negativos.");
			}
			this.mbDeAplicacion.add(entrada.nextDouble());
 	      }
		
 	  
 	  //cerrar Archivo
 	  
		entrada.close();
	
		
	}

//proceso

public int cantidadMinimaDeAppABorrar() throws RestriccionVioladaException {
	
	if(this.cantidadDeAplicacionesInstaladas>5000){
		throw new RestriccionVioladaException("Cantidad de apps invalida.");
	}
	if(this.cantidadDeMemoriaNecesaria>1000) {
		throw new RestriccionVioladaException("la aplicacion es muy pesada para el celular");
	}else {
		double SumaMin=1000;
		double aux=0;
		int cantApps=0;
		double min=0;
		ArrayList <Integer> cantidadesAEliminar= new ArrayList<Integer>();
		
		
		for(i=0;i<this.mbDeAplicacion.size();i++) {
			aux=this.mbDeAplicacion.get(i);
			cantApps=0;
			if(aux>=this.cantidadDeMemoriaNecesaria) {
				
				return 1;
			}
				//crear archivo salida
			
			for(j=i+1;j<this.mbDeAplicacion.size();j++) {
				
				aux+=this.mbDeAplicacion.get(j);
				cantApps++;
				
				if(aux>=this.cantidadDeMemoriaNecesaria) {
					
					cantidadesAEliminar.add(cantApps+1);
					
					break;
					
				}
				
				
			}
		}
		
		Collections.sort(cantidadesAEliminar);
		
		int minimoEliminar=cantidadesAEliminar.get(0);
		
		return minimoEliminar;
		
	}
	
}

//salida archivo

//crear archivo de salida
public static void crearArchivosSalida(int cantidad) throws IOException {
	PrintWriter salida = new PrintWriter(new FileWriter("Archivo/almacenamiento.out"));  //  preparo el arch de salida
    
  
    salida.print(cantidad);      
	
	salida.close(); //esto escribe : dni: 50
	}}