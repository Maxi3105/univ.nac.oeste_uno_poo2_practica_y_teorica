package SenialAudio;

import java.util.ArrayList;

/*Una se�al de audio digitalizada puede representarse como un arreglo de enteros, que oscilan entre �0� (silencio) y �65536� (tono agudo).
Se desea construir un software que dada la se�al, aplique un filtro pasa-banda definido por dos tonos (inferior y superior), 
que deja en silencio todo valor fuera de ese rango. El resultado debe compactarse, eliminando los silencios generados por este filtro.

Nota 1: Es estrictamente necesario aplicar el filtro primero, y luego compactar la se�al.  
Nota 2: No debe perderse la se�al original.
*/

public class Se�alAudio {
	private ArrayList <Integer> senial;

	public void SenialAudio(ArrayList<Integer> senial) {
		
		this.setSenial(senial);
	}
	
	public void filtroPasaBanda(ArrayList<Integer> senial, Integer inferior, Integer superior) {
		
		ArrayList<Integer> digital=new ArrayList<Integer>();
		
		for(int i=0;i<senial.size();i++) {
			
			if((senial.get(i)>=inferior)&&(senial.get(i)<=superior)) {
				
				digital.add(senial.get(i));
				
			}
			
			
		}
		
		for(int n=0;n<digital.size();n++) {
			
			System.out.println("se�al digital: "+digital.get(n));
		}
		
		
	}
	
	public ArrayList<Integer> getSenial() {
		return senial;
	}

	public void setSenial(ArrayList<Integer> senial) {
		this.senial = senial;
	}

	
	public static void main(String args[]) {
		
		ArrayList<Integer> intes=new ArrayList<Integer>(); 
		intes.add(5666);
		intes.add(50);
		intes.add(2000);
		intes.add(20000);
		intes.add(665871);
		intes.add(5000);
		intes.add(678858);
		intes.add(4875);
		intes.add(6879654);
		intes.add(546464);
		
		Se�alAudio sen= new Se�alAudio();

		sen.filtroPasaBanda(intes, 100, 700000);
		
	}
	
}
