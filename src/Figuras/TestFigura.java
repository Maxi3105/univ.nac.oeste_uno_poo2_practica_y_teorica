package Figuras;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import sun.jvm.hotspot.utilities.Assert;

class TestFigura {

	@Test
	public void testDesplazar() {
		Punto2D p=new Punto2D(1,1);
		p.desplazar(1, 1);
		Assert.assertEquals(p,new Punto2D(2,2));
	}
	
	@Test
	public void testAreaElipse() {
		Punto2D centro=new Punto2D(3,3);
		Figura elipse=new Elipse(centro,2,3);
		Assert.assertEquals(elipse.calcularArea(),Math.PI*3*2,0.1);
		elipse.desplazar(-2, -1);
		Assert.assertEquals(elipse.calcularArea(),Math.PI*3*2,0.1);		
	}
	
	@Test
	public void testAreaTrianguloPunto() {
		Punto2D v1=new Punto2D(3,3);
		Punto2D v2=new Punto2D(3,3);
		Punto2D v3=new Punto2D(3,3);
		Figura triangulo=new Triangulo(v1,v2,v3);
		Assert.assertEquals(triangulo.calcularArea(),0,0.1);
		triangulo.desplazar(-2, -1);
		Assert.assertEquals(triangulo.calcularArea(),0,0.1);		
	}
	
	@Test
	public void testAreaTrianguloSegmento() {
		Punto2D v1=new Punto2D(3,3);
		Punto2D v2=new Punto2D(3,4);
		Punto2D v3=new Punto2D(3,5);
		Figura triangulo=new Triangulo(v1,v2,v3);
		Assert.assertEquals(triangulo.calcularArea(),0,0.1);
		triangulo.desplazar(-2, -1);
		Assert.assertEquals(triangulo.calcularArea(),0,0.1);		
	}
	
	@Test
	public void testAreaTrianguloSobreY() {
		Punto2D v1=new Punto2D(0,-4);
		Punto2D v2=new Punto2D(0,4);
		Punto2D v3=new Punto2D(2,2);
		Figura triangulo=new Triangulo(v1,v2,v3);
		Assert.assertEquals(8,triangulo.calcularArea(),0.1);
		triangulo.desplazar(-2, -1);
		Assert.assertEquals(8,triangulo.calcularArea(),0.1);		
	}
	
	public void testAreaTrianguloSobreX() {
		Punto2D v1=new Punto2D(-4,0);
		Punto2D v2=new Punto2D(4,0);
		Punto2D v3=new Punto2D(0,4);
		Figura triangulo=new Triangulo(v1,v2,v3);
		Assert.assertEquals(32,triangulo.calcularArea(),0.1);
		triangulo.desplazar(-2, -1);
		Assert.assertEquals(32,triangulo.calcularArea(),0.1);		
	}

}
