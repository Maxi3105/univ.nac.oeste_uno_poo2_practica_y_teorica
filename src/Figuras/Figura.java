package Figuras;

public abstract class Figura implements Comparable<Figura>, Desplazable{
	
	protected Punto2D p1;
	

	public Figura(Punto2D p) {
		this.p1=p;
	}
	
	public abstract double calcularArea();

	@Override
	public int compareTo(Figura o) {
		if(this.calcularArea()<o.calcularArea()) {
		return -1;
		}
		if(this.calcularArea()>o.calcularArea()) {
			return 1;
		}
		return 0;
	}
	
	

}
