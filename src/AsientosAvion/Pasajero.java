package AsientosAvion;

public class Pasajero {
	
	private int dni;
	private String nombre;
	
	
	public Pasajero(int dni, String nombre) {
		super();
		this.dni = dni;
		this.nombre = nombre;
	}


	public int getDni() {
		return dni;
	}


	public String getNombre() {
		return nombre;
	}
	
	
	
	

}
