package Palindromo;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

public class Palindromo {
	private String cadena;
	private int i;
	private int j;
	
	//lectura
	
	public  void leerArchivo() throws NumberFormatException, IOException {
		
		File f = new File("Archivos/PALIN.in");
    	
		Scanner entrada = new Scanner(f);
 	    
 	    
 	  this.cadena= entrada.nextLine();
 	    
 	  
		entrada.close();
	System.out.println(cadena);
		
	}
	
	//proceso
	
	public static boolean esPalindromo(String cadenaAverificar) {
		boolean palindromo=false;
	
		cadenaAverificar.toLowerCase();
		
		String sCadenaInvertida="";
		for (int x=cadenaAverificar.length()-1;x>=0;x--) {
			  sCadenaInvertida = sCadenaInvertida + cadenaAverificar.charAt(x);
	
		}
		
		if(cadenaAverificar.equals(sCadenaInvertida)) {
			
			palindromo=true;
		}
		return palindromo;	
	
	}
		
//Proceso
	
	public void descomponerPalabra() {
		//verificamos longitud de la cadena
		
		int longitudCadena=this.cadena.length();
		
		String ipalindromo=this.cadena.substring(longitudCadena/2, longitudCadena);
		String dpalindromo=this.cadena.substring(0, longitudCadena/2);
		
		
		
		ArrayList <String> arrayDePalindromos=new ArrayList<String>();
		
		char [] varCadena=new char[longitudCadena];
		varCadena=this.cadena.toCharArray();
		
		String tmpPalindromo="";
		String tmpPalindromo2="";
		int ultimaPosicion=0;
		
		
		
	for(i=0;i<longitudCadena;i++) {
			
		tmpPalindromo=tmpPalindromo+varCadena[i];
			
		if(tmpPalindromo.length()>2 && esPalindromo(tmpPalindromo)) {
		
				arrayDePalindromos.add(tmpPalindromo);
				ultimaPosicion=i;
				ipalindromo=this.cadena.substring(ultimaPosicion+1,longitudCadena);
				
			}
		
		}
		
	
	for(i=ultimaPosicion;i<longitudCadena;i++) {
		
	tmpPalindromo2=this.cadena.substring(i, longitudCadena);
	
	if(tmpPalindromo2.length()>2 && esPalindromo(tmpPalindromo2)) {
		
		arrayDePalindromos.add(tmpPalindromo2);
		
		dpalindromo=this.cadena.substring(0, ultimaPosicion+1);
	
		
	}

	}
	

	if(arrayDePalindromos.size()==2) {
		
		crearArchivoSalida(arrayDePalindromos, ipalindromo,dpalindromo);
	}else if(esPalindromo(this.cadena)){
		
		crearArchivoSalida(this.cadena);
		
	}else {
		
		crearArchivoSalida("NO ES PALINDROMO");
	}
	
	
	}

		
		
		
		
	private void crearArchivoSalida(String cadena2) {
		FileWriter flwriter = null;
		try {
			//crea el flujo para escribir en el archivo
			
			flwriter = new FileWriter("Archivos/PALIN.out");
			
			//crea un buffer o flujo intermedio 
			
			BufferedWriter bfwriter = new BufferedWriter(flwriter);
			
			
			
			bfwriter.write(cadena2+"  "+"palindromo"+"\n");
		
			
		
			//cierra el buffer intermedio
			bfwriter.close();

		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (flwriter != null) {
				try {//cierra el flujo principal
					flwriter.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	
}

	public static void crearArchivoSalida(ArrayList<String> palindromos, String iPalin, String dPalin) {
		FileWriter flwriter = null;
		try {
			//crea el flujo para escribir en el archivo
			
			flwriter = new FileWriter("Archivos/PALIN.out");
			
			//crea un buffer o flujo intermedio 
			
			BufferedWriter bfwriter = new BufferedWriter(flwriter);
			
			
			
			bfwriter.write(palindromos.get(0)+"  "+"palindromo"+"\n");
			bfwriter.write(iPalin+"  "+"i-palindromo"+"\n");
			bfwriter.write(dPalin+"  "+"d-palindromo"+"\n");
			bfwriter.write(palindromos.get(1)+"  "+"palindromo"+"\n");
			
		
			//cierra el buffer intermedio
			bfwriter.close();

		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (flwriter != null) {
				try {//cierra el flujo principal
					flwriter.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}}
		
		
		
	
		/*
		

	public static String  invertirCadena(String cadenaAinvertir) {
         cadenaAinvertir.toLowerCase();
		
		String cadenaInvertida="";
		for (int x=cadenaAinvertir.length()-1;x>=0;x--) {
			  cadenaInvertida = cadenaInvertida + cadenaAinvertir.charAt(x);
	
		}
		return cadenaInvertida;
		
		
		
	}
*/
	
	public static void main(String[] args) throws NumberFormatException, IOException {
		Palindromo P=new Palindromo();
		P.leerArchivo();
		P.descomponerPalabra();
	}
		
	
		
	
	

	

}
