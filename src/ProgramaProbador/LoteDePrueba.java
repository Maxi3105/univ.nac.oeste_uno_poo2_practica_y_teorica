package ProgramaProbador;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;

public class LoteDePrueba {
	
	public LoteDePrueba() {}
	
	public void generarLote(Integer cantidadDePacientes, String nombreSalida) throws IOException {
		
		double poscicionNom;
		double poscicionAp;
		String cadena;
		ArrayList<String> apellidos = new ArrayList<String>();
		ArrayList<String> nombres = new ArrayList<String>();
		ArrayList<Paciente> pacientes = new ArrayList<Paciente>();
		nombreSalida = "hClinicas"+nombreSalida;
		
		
	    FileReader f = new FileReader("Archivos/apellidos.txt");
	    BufferedReader b = new BufferedReader(f);
	    
	    while((cadena = b.readLine())!=null) {
	           apellidos.add(cadena);
	      }
	    
	    f = new FileReader("Archivos/nombres.txt");
	    b = new BufferedReader(f);
	    
	    while((cadena = b.readLine())!=null) {
	           nombres.add(cadena);
	      }
	    
	    b.close();
	    
		for(int i=1; i<=cantidadDePacientes; i++){
	    	 poscicionNom = Math.floor(Math.random()*(nombres.size()));
	         poscicionAp = Math.floor(Math.random()*(apellidos.size()));
	         cadena = nombres.get((int) poscicionNom)+" "+apellidos.get((int) poscicionAp);
	     	 pacientes.add(new Paciente(i, cadena));
	     }
		
		crearArchivo(pacientes, nombreSalida);
	}
	
	
	
	public  void crearArchivo(ArrayList<Paciente> pacientes, String nombreSalida) {
		FileWriter flwriter = null;
		try {
			//crea el flujo para escribir en el archivo
			flwriter = new FileWriter("Archivos/lote/"+nombreSalida+".out");
			//crea un buffer o flujo intermedio antes de escribir directamente en el archivo
			BufferedWriter bfwriter = new BufferedWriter(flwriter);
			for (int i=0;i<pacientes.size();i++ ) {
				//escribe los pacientes en el archivo
				bfwriter.write(pacientes.get(i).getNumHistoria()+" "+pacientes.get(i).getNombre()+ "\n");
			}
			//cierra el buffer intermedio
			bfwriter.close();
 
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (flwriter != null) {
				try {//cierra el flujo principal
					flwriter.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		
		Collections.shuffle(pacientes);    
		
		flwriter = null;
		try {
			//crea el flujo para escribir en el archivo
			flwriter = new FileWriter("Archivos/lote/"+nombreSalida+".in");
			//crea un buffer o flujo intermedio antes de escribir directamente en el archivo
			BufferedWriter bfwriter = new BufferedWriter(flwriter);
			bfwriter.write(pacientes.size()+ "\n");
			for (int i=0;i<pacientes.size();i++ ) {
				//escribe los pacientes en el archivo
				bfwriter.write(pacientes.get(i).getNumHistoria()+" "+pacientes.get(i).getNombre()+ "\n");
			}
			//cierra el buffer intermedio
			bfwriter.close();
			System.out.println("Archivo "+nombreSalida+" creado satisfactoriamente..");
 
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (flwriter != null) {
				try {//cierra el flujo principal
					flwriter.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		
	}
	
	
	//Comparar la salida del sistema con la salida esperada
    public boolean compararArchivos(String archivoUno, String archivoDos) throws IOException {
    
    	ArrayList<String> aarchivoUno = new ArrayList<String>();
    	ArrayList<String> aarchivoDos = new ArrayList<String>();
    	
    	FileReader f = new FileReader("Archivos/"+archivoUno);
    	
 	    BufferedReader b = new BufferedReader(f);
 	    
 	    String cadena;
		while((cadena = b.readLine())!=null) {
			aarchivoUno.add(cadena);
 	      }
 	    
 	    f = new FileReader("Archivos/"+archivoDos);
 	    b = new BufferedReader(f);
 	    
 	    while((cadena = b.readLine())!=null) {
 	    	aarchivoDos.add(cadena);
 	      }
 	    
 	    b.close();
 	    
    	return aarchivoUno.equals(aarchivoDos);
    }
	
    public static void ejecutarPrograma(String direccion, int repeticion ) {
    	
    	Runtime p = Runtime.getRuntime();
    	
    	int i;
    	
    	for(i=0;i>repeticion;i++) {
    	try
    	{
    	   /* direccion/ejecutable es el path del ejecutable y un nombre */
    		
    	   p.exec(direccion);
    	}
    	catch (Exception e)
    	{
    	   System.out.println(e);/* Se lanza una excepci�n si no se encuentra en ejecutable o el fichero no es ejecutable. */
    	}}
    	
    }
    public static void main(String[] args) throws IOException {
		LoteDePrueba lote = new LoteDePrueba();
		lote.generarLote(32000, "04");	
	//	lote.ejecutarPrograma();
		//System.out.println(lote.compararArchivos("test/1/hClinicas02.out", "test/1/hClinicas02-esperado.out"));
	}

}