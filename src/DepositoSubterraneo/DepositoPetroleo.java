package DepositoSubterraneo;

public class DepositoPetroleo {
	
	public int superficieBase;
	public int profundidad;
	public double capacidadcubica;
	
	
	public DepositoPetroleo(int superficieBase, int profundidad) {
		super();
		this.superficieBase = superficieBase;
		this.profundidad = profundidad;
		this.capacidadcubica=calcularVolumen(superficieBase,  profundidad);
		
	}
	
	//calculamos la capacidad de el deposito
	public static double calcularVolumen(int superficieBase, int altura) {
	
		double volumen;
		return  volumen=superficieBase*altura;

	}

	public int getSuperficieBase() {
		return superficieBase;
	}

	public void setSuperficieBase(int superficieBase) {
		this.superficieBase = superficieBase;
	}

	public int getProfundidad() {
		return profundidad;
	}

	public void setProfundidad(int profundidad) {
		this.profundidad = profundidad;
	}

	public double getCapacidadcubica() {
		return capacidadcubica;
	}

	public void setCapacidadcubica(double capacidadcubica) {
		this.capacidadcubica = capacidadcubica;
	}

	@Override
	public String toString() {
		return "DepositoPetroleo [superficieBase=" + superficieBase + ", profundidad=" + profundidad
				+ ", capacidadcubica=" + capacidadcubica + "]";
	}

	

}
