package MetodosOrdenamientoElementales;

public class ComparativaMetodosElementalesYsusAlgoritmos {

	
	public static  int [] burbujeo(int [] arreglo) {
		int auxiliar;
	      int[] arregloOrdenado;
	      for(int i = 2; i < arreglo.length; i++)
	      {
	        for(int j = 0;j < arreglo.length-i;j++)
	        {
	          if(arreglo[j] > arreglo[j+1])
	          {
	            auxiliar = arreglo[j];
	            arreglo[j] = arreglo[j+1];
	            arreglo[j+1] = auxiliar;
	          }   
	        }
	      }
	      arregloOrdenado = arreglo;
	      return arregloOrdenado;
		
		
	}
	
	public static int [] insercionDirecta(int A[]){
	    int p, j;
	    int aux;
	    for (p = 1; p < A.length; p++){ // desde el segundo elemento hasta
	              aux = A[p]; // el final, guardamos el elemento y
	              j = p - 1; // empezamos a comprobar con el anterior
	              while ((j >= 0) && (aux < A[j])){ // mientras queden posiciones y el
	                                                                    // valor de aux sea menor que los
	                             A[j + 1] = A[j];       // de la izquierda, se desplaza a
	                             j--;                   // la derecha
	              }
	              A[j + 1] = aux; // colocamos aux en su sitio
	    }
		return A;
	}
	
	//m�todo java de ordenaci�n por selecci�n
	public static int []  seleccion(int A[]) {
	          int i, j, menor, pos, tmp;
	          for (i = 0; i < A.length - 1; i++) { // tomamos como menor el primero
	                menor = A[i]; // de los elementos que quedan por ordenar
	                pos = i; // y guardamos su posici�n
	                for (j = i + 1; j < A.length; j++){ // buscamos en el resto
	                      if (A[j] < menor) { // del array alg�n elemento
	                          menor = A[j]; // menor que el actual
	                          pos = j;
	                      }
	                }
	                if (pos != i){ // si hay alguno menor se intercambia
	                    tmp = A[i];
	                    A[i] = A[pos];
	                    A[pos] = tmp;
	                }
	          }
			return A;
	}
	
	public static void medirTiempoEjecucion(int [] vectorAComparar) {

			  long TInicio, TFin, tiempo; //Variables para determinar el tiempo de ejecuci�n
			  long TInicio2, TFin2, tiempo2;
			  long TInicio3, TFin3, tiempo3;
			  
			  int[] vectorAux = new int[vectorAComparar.length];
			  vectorAux=vectorAComparar;
			//burbujeo
			  TInicio = System.currentTimeMillis(); //Tomamos la hora en que inicio el algoritmo y la almacenamos en la variable inicio
			
			  burbujeo(vectorAComparar);

			TFin = System.currentTimeMillis(); //Tomamos la hora en que finaliz� el algoritmo y la almacenamos en la variable T
			  tiempo = TFin - TInicio; //Calculamos los milisegundos de diferencia
			  System.out.println("Tiempo de ejecuci�n de burbujeo en milisegundos: " + tiempo); //Mostramos en pantalla el tiempo de ejecuci�n en milisegundos
			 
		//seleccion
			  vectorAComparar=vectorAux;
			  TInicio2 = System.currentTimeMillis(); //Tomamos la hora en que inicio el algoritmo y la almacenamos en la variable inicio
				
			  seleccion(vectorAComparar);

			  TFin2 = System.currentTimeMillis(); //Tomamos la hora en que finaliz� el algoritmo y la almacenamos en la variable T
			  tiempo2 = TFin2 - TInicio2; //Calculamos los milisegundos de diferencia
			  System.out.println("Tiempo de ejecuci�n de seleccion en milisegundos: " + tiempo2); //Mostramos en pantalla el tiempo de ejecuci�n en milisegundos
			 
		//insercionDirecta
			  
			  vectorAComparar=vectorAux;
			  
			  TInicio3 = System.currentTimeMillis(); //Tomamos la hora en que inicio el algoritmo y la almacenamos en la variable inicio
				
			  insercionDirecta(vectorAComparar);

			  TFin3 = System.currentTimeMillis(); //Tomamos la hora en que finaliz� el algoritmo y la almacenamos en la variable T
			  tiempo3 = TFin3 - TInicio3; //Calculamos los milisegundos de diferencia
			  System.out.println("Tiempo de ejecuci�n de Insercion directa en milisegundos: " + tiempo3); //Mostramos en pantalla el tiempo de ejecuci�n en milisegundos
			 
			  if(tiempo<tiempo2 && tiempo<tiempo3) {
				  
				  System.out.println("el mejor metodo de busqueda es burbujeo");
			  }else if(tiempo2<tiempo && tiempo2<tiempo3) {
				  System.out.println("el mejor metodo de busqueda es seleccion");
			  }else {
				  System.out.println("el mejor metodo de busqueda es insercion");
			  }
		
	}
public static void main(String[] args) {
		
		int i;
			
		int[] vectorNumeros = new int[10000];

		for(i=0;i<10000;i++) {
			vectorNumeros[i]=(int) Math.random();
		
		}
		
		ComparativaMetodosElementalesYsusAlgoritmos.medirTiempoEjecucion(vectorNumeros);
	}
}
