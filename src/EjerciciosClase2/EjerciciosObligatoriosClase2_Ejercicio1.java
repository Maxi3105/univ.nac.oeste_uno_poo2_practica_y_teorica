package EjerciciosClase2;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class EjerciciosObligatoriosClase2_Ejercicio1 {

	/*Ejercicio 1:

Dado un conjunto de n�mero enteros mayores o iguales a 0 y menores que 100 determinar 
e informar cu�ntas veces aparece cada uno. 
El conjunto finaliza con la llegada de un valor negativo.*/
	
	private Integer num;
	
	private ArrayList<Integer> arrayNumeros;
	
	



	public EjerciciosObligatoriosClase2_Ejercicio1() {
		super();
		
	}
		
	

	public ArrayList <Integer> ingresarNumeros() {
		
		ArrayList<Integer> arrayNumeros = new ArrayList();
		
		num=1;
		
		do {
			
			System.out.println("iNGRESE UN NUMERO >0 && <100");
			System.out.println("PARA SALIR INGRESE 0");
			
			Scanner sc1 = new Scanner (System.in);
			
			num=sc1.nextInt();
			
			if(num!=0) {
				arrayNumeros.add(num);
			}
			
	
		}while(num!=0);
		
		
		
		return arrayNumeros;
		
	
	}
	
	
	public void mostrarArray(ArrayList <Integer> arrayNumero) {
		int i;
		for(Integer tmp: arrayNumero) {
			
			System.out.print(tmp+" ; ");
		}
		
		System.out.println();
		
	}
	
	public void MostrarRepetidosArray(ArrayList <Integer> arrayNumero) {
		
		
		int i;
		int j;
		Map<Integer, Integer> map = new HashMap<Integer, Integer>();
		
		for (i=0;i<arrayNumero.size();i++) {
			
			Integer cont =0;
			for(j=0;j<arrayNumero.size();j++) {
				
				if(arrayNumero.get(i).equals(arrayNumero.get(j))) {
					cont++;
				    map.put(arrayNumero.get(i),cont);
					
				}
			}
			
			
		}
		
		
		for (Map.Entry<Integer, Integer> entry : map.entrySet()) {
		    System.out.println("Numero=" + entry.getKey() + ", Aparece=" + entry.getValue()+" Veces.");
		}
		
		
	
	}
	

	
	public static void main(String[] args) {
		
		EjerciciosObligatoriosClase2_Ejercicio1 E1C2= new EjerciciosObligatoriosClase2_Ejercicio1();
		
		
		
		ArrayList<Integer> arrayAMostrar = new ArrayList();
		
		arrayAMostrar=E1C2.ingresarNumeros();
		
		E1C2.mostrarArray(arrayAMostrar);
		
		E1C2.MostrarRepetidosArray(arrayAMostrar);
		
		
	}





	





	

	
}
