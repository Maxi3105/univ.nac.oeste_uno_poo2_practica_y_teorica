package EjerciciosClase2;

import java.util.ArrayList;

public class EjerciciosObligatoriosClase2_Ejercicio2 {
	
	/*Ejercicio 2:

Una señal de audio digitalizada puede representarse como un arreglo de enteros, que oscilan entre ‘0’ 
(silencio) y ‘65536’ (tono agudo).
Se desea construir un software que dada la señal,
 aplique un filtro pasa-banda definido por dos tonos (inferior y superior), que deja en silencio todo valor 
 fuera de ese rango. El resultado debe compactarse, eliminando los silencios generados por este filtro.
       
 Nota 1: Es estrictamente necesario aplicar el filtro primero, y luego compactar la señal.  
 Nota 2: No debe perderse la señal original.*/

	private static final int media=32768;
	
	public ArrayList<Integer> crearSeñalDigital(){
	
	ArrayList<Integer> señalDigital=new ArrayList<Integer>();
	
	int i;
	int valorTmp;
	for (i=0;i<2000;i++) {
		valorTmp=(int) Math.floor(Math.random()*65536+1);
		señalDigital.add(valorTmp);
	
	}
	
	return señalDigital;

}
	
	public ArrayList<Integer> filtroSeñal(ArrayList<Integer> señalDigital) {
		ArrayList<Integer> señalDigitalCompactada=new ArrayList<Integer>();
		ArrayList<Integer> bandaInferior=new ArrayList<Integer>();
		ArrayList<Integer> bandaSuperior=new ArrayList<Integer>();
		
		for(Integer tmp : señalDigital) {
			
			if(tmp<media) {
				if(tmp!=0) {
					bandaInferior.add(tmp);
				}
				
			}else {
				bandaSuperior.add(tmp);
			}
		}
		
		return señalDigitalCompactada=compactar(bandaInferior, bandaSuperior);
		
		
	}
	
	
	private  ArrayList<Integer> compactar(ArrayList<Integer> bandaInferior,ArrayList<Integer> bandaSuperior) {
		
		ArrayList<Integer> señalDigitalCompactada=new ArrayList<Integer>();
		
		
		for(Integer tmp : bandaInferior) {
			
			señalDigitalCompactada.add(tmp);
		}
		for(Integer tmp: bandaSuperior) {
			
			señalDigitalCompactada.add(tmp);
		}
		
		
		
		return señalDigitalCompactada;
		
		
	}

	public static void main(String[] args) {
		
		
		
	}
	

		
		
		
	}
	

